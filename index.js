// inicjacja funkcji, kt�ra b�dzie handlerem dla HTTP serwera
var app = require('express')();

// HTTP serwera
var serverHttp = require('http').Server(app);
var transports = ['polling','websocket']; // 'polling','websocket'
var io = require('socket.io')(serverHttp, {
        pingInterval: 5000, // co 25 sek ping po odpowied� pong  (ping-pong)
        pingTimeout:  1000, // max 5 sek czekam na pong (response) pakiet
        cookie: false,
        upgradeTimeout: 10000, // 1 or 10  - maksymalny czas na upgrade transportu
        allowUpgrades: true, // false - transport is poling , true allow upgrades
        transport: transports// 'polling', 'websocket'
		// kolejnos� jest B. wazna  polling jest ustanowiony jako pierwszy, a dopiero potem je�li mo�liwe transport aktualizowny do websocket. Nie bedzie odpoweidzi je�li websocket po�aczenie nie zostanie ustanowione.
        
        // more options in docs: https://github.com/socketio/engine.io#methods-1
    });
	



io.on('connection', function (socket) {
	
	// zobaczmy pod��czaonych klient�w ....
	io.clients((error, clients) => {
        if (error)
            throw error;
        console.log(clients);
		//console.log(socket.handshake.query);   /// HANDSHAKE
    });

    socket.on('disconnect', function () {
		//io.emit('bye', 'Bye!');
		io.clients((error, clients) => {
            if (error)
                throw error;
			console.log("----------------disconnect--------------------");
            console.log(clients);
			
        });
    });
	
	socket.on('error', (error) => {
		// here handle some ugly errors, durring connection ..
	});
	
	//## komunikacja (klient-serwer)
	
	socket.on('chat message', function (msg) {
        io.volatile.emit('chat message', msg); // do wszystkich ��cznie ze mn�
		socket.broadcast.emit('chat message', msg); //  do wszystkich, ale nie do mnie
	});
});



//######################################### namespaces
/**var adminNamespace = io.of('/admin');

adminNamespace.on('connection', function (socket) {
	
    console.log("admin connected");

    socket.on('disconnect', function () {
        console.log("admin lost his mind");
    });
	
    socket.on('disconnecting', (reason) => {
        let rooms = Object.keys(socket.rooms);
        console.log(reason + "  " + rooms);
    });
});
*/

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});
serverHttp.listen(3009, function () {
    console.log('listening on *:3009');
});